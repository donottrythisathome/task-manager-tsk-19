package com.ushakov.tm.comparator;

import com.ushakov.tm.api.model.IHasStatus;

import java.util.Comparator;

public final class StatusComparator implements Comparator<IHasStatus> {

    private static final StatusComparator INSTANCE = new StatusComparator();

    private StatusComparator() {
    }

    public static StatusComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasStatus o1, final IHasStatus o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
