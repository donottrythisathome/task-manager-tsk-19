package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(list);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project: list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        try {
            return list.get(index);
        } catch(Exception e) {
            throw new ProjectNotFoundException();
        }
    }

}
