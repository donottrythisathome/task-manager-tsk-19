package com.ushakov.tm.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    public List<E> findAll() {
        return list;
    }

    public E add(final E entity) {
        list.add(entity);
        return entity;
    }

    public void remove(final E entity) {
        list.remove(entity);
    }

    public void clear() {
        list.clear();
    }

    public E findOneById(final String id) {
        for (final E entity: list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    public E removeOneById(final String id) {
        final E entity = findOneById(id);
        if (entity == null) throw new ObjectNotFoundException();
        remove(entity);
        return entity;
    }

}
