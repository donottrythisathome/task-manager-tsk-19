package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.ICommandRepository;
import com.ushakov.tm.api.service.ICommandService;
import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.model.Command;

import java.util.List;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public List<String> getCommandNames() {
        return commandRepository.getCommandNames();
    }

}
