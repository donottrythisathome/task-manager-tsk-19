package com.ushakov.tm.service;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.api.IService;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E add(E entity) {
        if (entity == null) throw new ObjectNotFoundException();
        return repository.add(entity);
    }

    @Override
    public void remove(E entity) {
        if (entity == null) throw new ObjectNotFoundException();
        repository.remove(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E findOneById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findOneById(id);
    }

    @Override
    public E removeOneById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeOneById(id);
    }

}
