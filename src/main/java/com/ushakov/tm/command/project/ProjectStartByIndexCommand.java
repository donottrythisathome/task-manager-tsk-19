package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "start-project-by-index";
    }

    @Override
    public String description() {
        return "Start project by index.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        final Project project = serviceLocator.getProjectService().startProjectByIndex(projectIndex - 1);
        if (project == null) throw new ProjectNotFoundException();
    }

}
