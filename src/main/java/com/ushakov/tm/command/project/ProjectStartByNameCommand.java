package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "start-project-by-name";
    }

    @Override
    public String description() {
        return "Start project by name.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startProjectByName(projectName);
        if (project == null) throw new ProjectNotFoundException();
    }

}
