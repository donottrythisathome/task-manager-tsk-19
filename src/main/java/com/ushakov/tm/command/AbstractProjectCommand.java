package com.ushakov.tm.command;

import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
        System.out.println("Start Date: " + project.getDateStart());
        System.out.println("Finish Date: " + project.getDateEnd());
        System.out.println("Created: " + project.getCreated());
    }

}
