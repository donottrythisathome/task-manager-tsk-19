package com.ushakov.tm.command.auth;

import com.ushakov.tm.command.AbstractUserCommand;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout as a current user.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]:");
        serviceLocator.getAuthService().logout();
    }

}
