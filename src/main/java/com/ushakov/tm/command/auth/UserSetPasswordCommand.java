package com.ushakov.tm.command.auth;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.TerminalUtil;

public class UserSetPasswordCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-set-password";
    }

    @Override
    public String description() {
        return "Set user password.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER ID:");
        final String userId = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        if (serviceLocator.getUserService().setPassword(userId, password) == null) {
            throw new UserNotFoundException();
        }
    }

}
