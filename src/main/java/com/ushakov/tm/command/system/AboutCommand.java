package com.ushakov.tm.command.system;

import com.ushakov.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ivan Ushakov");
        System.out.println("E-MAIL: iushakov@tsconsulting.com \n");
    }

}
