package com.ushakov.tm.command.system;

import com.ushakov.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "Terminate console application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
