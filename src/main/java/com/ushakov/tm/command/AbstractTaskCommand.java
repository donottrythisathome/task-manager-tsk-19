package com.ushakov.tm.command;

import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showProject(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Start Date: " + task.getDateStart());
        System.out.println("Finish Date: " + task.getDateEnd());
        System.out.println("Created: " + task.getCreated());
    }

}
