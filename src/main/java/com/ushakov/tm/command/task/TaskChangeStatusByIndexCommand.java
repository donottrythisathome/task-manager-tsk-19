package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "change-task-status-by-index";
    }

    @Override
    public String description() {
        return "Change task status by index.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = serviceLocator.getTaskService().changeTaskStatusByIndex(taskIndex-1, status);
        if (task == null) throw new TaskNotFoundException();
    }

}
