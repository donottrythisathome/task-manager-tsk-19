package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "complete-task-by-id";
    }

    @Override
    public String description() {
        return "Complete task by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().completeTaskById(taskId);
        if (task == null) throw new TaskNotFoundException();
    }

}
