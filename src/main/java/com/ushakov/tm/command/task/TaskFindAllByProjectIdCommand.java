package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "find-tasks-by-project-id";
    }

    @Override
    public String description() {
        return "Find all tasks related to project.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> projectTaskList = serviceLocator.getProjectTaskService().findAllTasksByProjectId(projectId);
        if (projectTaskList == null) throw new TaskNotFoundException();
        int index = 1;
        for (final Task task: projectTaskList) {
            System.out.println(index + ": " +task);
            System.out.println();
            index++;
        }
    }
}
