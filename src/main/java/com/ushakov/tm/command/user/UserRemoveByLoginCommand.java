package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().removeOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
    }
}
