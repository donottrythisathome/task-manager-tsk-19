package com.ushakov.tm.model;

import com.ushakov.tm.api.model.IWBS;
import com.ushakov.tm.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractEntity implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    public String projectId;

    private Date dateStart;

    private Date dateEnd;

    private Date created = new Date();

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "ID: " + this.getId() + "\nNAME: " + name + "\nDESCRIPTION: " + description
                + "\nSTATUS: " + status.getDisplayName() + "\nPROJECT ID:" + projectId
                + "\nSTART DATE: " + dateStart + "\nCREATED: " + created;
    }

}
