package com.ushakov.tm.api.service;

import com.ushakov.tm.api.IService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description);

    Project removeOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project findOneByName(String name);

    Project findOneByIndex(Integer index);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project updateProjectById(String id, String name, String description);

    Project startProjectById(String id);

    Project startProjectByName(String name);

    Project startProjectByIndex(Integer index);

    Project completeProjectById(String id);

    Project completeProjectByName(String name);

    Project completeProjectByIndex(Integer index);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByName(String name, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
