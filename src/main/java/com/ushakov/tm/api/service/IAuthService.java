package com.ushakov.tm.api.service;

import com.ushakov.tm.model.User;

public interface IAuthService {

    void login(final String login, final String password);

    void logout();

    User getUser();

    String getUserId();

    boolean isAuth();

    void registry(final String login, final String password, final  String email);

}
