package com.ushakov.tm.api;

import com.ushakov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    E add(E entity);

    E findOneById(String id);

    void clear();

    E removeOneById(String id);

    void remove(E entity);

}
