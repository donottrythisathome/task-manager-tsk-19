package com.ushakov.tm.api.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.model.AbstractEntity;
import com.ushakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAll(Comparator<Task> comparator);

    Task removeOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

}
